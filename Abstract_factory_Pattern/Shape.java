package Abstract_factory_Pattern;

public interface Shape {
	void draw();
}
