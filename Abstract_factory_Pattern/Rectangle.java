package Abstract_factory_Pattern;

public class Rectangle implements Shape{
	@Override
	   public void draw() {
	      System.out.println("Inside Rectangle::draw() method.");
	   }
}
